'use strict';

// simple utility functions
module.exports = {

    // simple helper function for use with nconf reading from a config file.
    // function checks if a parameter passed in is undefined and return a default value that could be used to set the parm to a default.
    // can be used as follows to define a default if the value was not set in the config file
    // const configParm = checkConfigForUndefinedAndSetDefault(nconf.get('group.id'), 'Example');
     checkConfigForUndefinedAndSetDefault : function (configParm, defaultValue) {
        return (typeof configParm == 'undefined') ? defaultValue : configParm;
    }
    
};

'use strict';

/*
    Implementation of Winston logging to be used by all files in js project - this will log to the console
    and to a File for all logger calls invoked in all modules of a project.

    Usage: 
        In project js files:
            const logger=require('./lib/log.js'); 

            logger.info etc...
*/

const winston = require('winston');

// * add function to create log file name from logs/process_id/cuid to make it unique
function createLogFileName() {
    const cuid = require('cuid');
    const c2 = cuid();
    const procid = process.pid;
    return `./logs/logfile_${procid}_${c2}.log`;
}
const myLogFile = createLogFileName();
console.log('log file name = ', myLogFile);

// code to verify that the logs directory exists or create it if it doesn't
const fs = require('fs');
fs.stat("./logs", function (err, stats){
    if (err) {
      // Directory doesn't exist or something.
      fs.mkdirSync("./logs");
    }
});

var logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: `${myLogFile}` })
    ]
});    
    
module.exports=logger;
'use strict';

var kafka = require('node-rdkafka');
const utils = require('./lib/utils.js');
// set up winston logging to the console and to the named log file
const logger=require('./lib/log.js'); 
const cuid = require('cuid');

// * process configuration file name from command line
if (process.argv.length <= 3) {
  console.log("\n\nUsage: node kafka-producer.js config_file_name num_messages_to_send ");
  console.log("Example: node kafka-producer.js simple-1-producer.json 20");
  console.log("Supported Configurations: \n\tsimple-1-producer.json: Simple Producer for Message send to topic identifed in config file.\n")
  console.log("\tmultiple-msg-producer-2partition.json: Producer for sending messages to a topic that has 2 partitions. \n")
  process.exit(-1);
}
const fname = `./conf/${process.argv[2]}`;
const numMessagesToSend = process.argv[3];

// * use nconf for configuration
const nconf = require('nconf');
nconf.file(`./conf/${process.argv[2]}`);

const hostAddress = utils.checkConfigForUndefinedAndSetDefault(nconf.get('metadata.broker.list'), 'localhost:9092');
const topicName = utils.checkConfigForUndefinedAndSetDefault(nconf.get('topicName'), 'search-request');
const clientId = utils.checkConfigForUndefinedAndSetDefault(nconf.get('client.id'), 'rdkafka');
const compressionCodec = utils.checkConfigForUndefinedAndSetDefault(nconf.get('compression.codec'), 'gzip');
const retryBackoff = utils.checkConfigForUndefinedAndSetDefault(nconf.get('retry.backoff.ms'), 200);
const messageSendMaxRetries = utils.checkConfigForUndefinedAndSetDefault(nconf.get('message.send.max.retries'), 10);
const socketKeepaliveEnable = utils.checkConfigForUndefinedAndSetDefault(nconf.get('socket.keepalive.enable'), true);
const queueBufferingMaxMsgs = utils.checkConfigForUndefinedAndSetDefault(nconf.get('queue.buffering.max.messages'), 100000);
const queueBufferingMaxMs = utils.checkConfigForUndefinedAndSetDefault(nconf.get('queue.buffering.max.ms'), 1000);
const batchNumMessages = utils.checkConfigForUndefinedAndSetDefault(nconf.get('batch.num.messages'), 1000000);
const drCb = utils.checkConfigForUndefinedAndSetDefault(nconf.get('dr_cb'), false);


logger.info('Required Parameters in configuration. If test is not working then check config is correct: ');
logger.info('Host Address: ' + hostAddress);
logger.info('Topic Name: ' + topicName);

var producer = new kafka.Producer({
    'client.id': clientId,
    'metadata.broker.list': hostAddress,
    'compression.codec': compressionCodec,
    'retry.backoff.ms':  retryBackoff,
    'message.send.max.retries': messageSendMaxRetries,
    'socket.keepalive.enable': socketKeepaliveEnable,
    'queue.buffering.max.messages': queueBufferingMaxMsgs,
    'queue.buffering.max.ms': queueBufferingMaxMs,
    'batch.num.messages': batchNumMessages,
    'dr_cb': drCb
  });

  // builds a JSON formatted string that can be used in a message send
function buildMessage(msgNum) {
  
  return ("{\"message_number\" : \"" + msgNum + "\", " +
          "\"request_id\" : \"" + cuid() + "\", " +
          "\"claim_number\" : \"Claim123456789001\", " +
          "\"lob\" : \"Claims_East\", " +
          "\"userid\" : \"nordj\"}");

}

function sendMessage(msgNum) {
    let valuemsg = buildMessage(msgNum);
    logger.info('going to send a message', valuemsg);
    producer.produce(
      // Topic to send the message to
      topicName,
      // optionally we can manually specify a partition for the message
      // this defaults to -1 - which will use librdkafka's default partitioner (consistent random for keyed messages, random for unkeyed messages)
      null,
      // Message to send. Must be a buffer
      new Buffer(valuemsg),
          // for keyed messages, we also specify the key - note that this field is optional
          //'Stormwind',
          // you can send a timestamp here. If your broker version supports it,
          // it will get added. Otherwise, we default to 0
          Date.now(),
          // you can send an opaque token here, which gets passed along
          // to your delivery reports
    );
}

// Connect to the broker manually
producer.connect(); 
                           
// Wait for the ready event before proceeding
producer.on('ready', function() {
  try {
    for (let i = 0; i < numMessagesToSend; i++) {
      sendMessage(i);
    }
  } catch (err) {
    logger.error('A problem occurred when sending our message');
    logger.error(err);
  }
});

// Poll for events every 100 ms
producer.setPollInterval(100);
 
producer.on('delivery-report', function(err, report) {
  // Report of delivery statistics here:
  logger.info('delivery-report', report);
  //producer.disconnect();
});
// Any errors we encounter, including connection errors
producer.on('event.error', function(err) {
  logger.error('Error from producer');
  logger.error(err);
}); 


// Close on Ctrl-C
process.on('SIGINT', () => {
    logger.info("Shutting down ...");
    producer.disconnect();
});

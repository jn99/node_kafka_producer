Kafka Producer written in Node.js using node-rdkafka library.

this Producer writes messages to the topic defined in the conf file identified as a command line paramenter.

Usage:  node kafka-producer.js config_file_name num_messages_to_send 

Config files are in conf directory. They define parameters such as the broker address and the name of the topic.

Note: program assumes config file is in ./conf directory and ./conf is not required in the parameter.
Example: node kafka-prodicer.js producer-send-onePartition.json


Currently supported configurations:

    producer-send-onePartition.json - sends to topic named onePartition. This was created with the following:
        /usr/local/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic onePartition

    producer-send-twoPartition.json - sends to topic named twoPartition. This was created with the following:
        /usr/local/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 2 --topic onePartition
   
    multiple-msg-producer-2partition.json: sends to a topic with 2 partitions. This allows for 2 consumers to read from the topic and spread the load. 

note that to install the node-rdkafka npm project you need to do this:

export CPPFLAGS=-I/usr/local/opt/openssl/include

export LDFLAGS=-L/usr/local/opt/openssl/lib

then npm install node-rdkafka

